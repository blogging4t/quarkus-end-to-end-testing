package ro.blogging.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.blogging.entity.CommentEntity;
import ro.blogging.entity.PostEntity;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.UUID;

public interface CommentRepository extends CrudRepository<CommentEntity, UUID> {

    Page<CommentEntity> getAllByPostEntity(final PostEntity postEntity, final Pageable pageable);

    @Transactional
    @Modifying
    @Query("update CommentEntity c set c.title = ?1, c.content = ?2, c.author = ?3" +
            ",c.updateTime =?4  where c.id = ?5")
    int updateComment(final String title, final String content, final String author, final ZonedDateTime updateTime,
                      final UUID postId);
}
