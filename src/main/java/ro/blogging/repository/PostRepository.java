package ro.blogging.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.blogging.entity.PostEntity;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.UUID;

public interface PostRepository extends CrudRepository<PostEntity, UUID> {


    Page<PostEntity> findAll(final Pageable pageable);

    @Transactional
    @Modifying
    @Query("update PostEntity p set p.title = ?1, p.content = ?2, p.author = ?3" +
            ",p.updateTime =?4  where p.id = ?5")
    int updatePost(final String title, final String content, final String author, final ZonedDateTime updateTime,
                   final UUID postId);
}
