package ro.blogging.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.auto.value.AutoValue;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AutoValue
@RegisterForReflection
@JsonPropertyOrder({"id", "pageRequest"})
public abstract class GetCommentsRequestTO {

    @NotNull
    public abstract UUID getPostId();

    @NotNull
    public abstract PageRequestTO getPageRequest();

    @JsonCreator
    public static GetCommentsRequestTO create(@JsonProperty("postId") final UUID postId,
                                              @JsonProperty("pageRequest") final PageRequestTO pageRequest) {
        return builder()
                .postId(postId)
                .pageRequest(pageRequest)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_GetCommentsRequestTO.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder postId(final UUID postId);

        public abstract Builder pageRequest(@Valid final PageRequestTO newPageRequest);

        public abstract GetCommentsRequestTO build();
    }
}
