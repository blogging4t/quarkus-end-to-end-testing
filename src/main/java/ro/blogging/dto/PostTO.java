package ro.blogging.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.auto.value.AutoValue;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.reactivex.annotations.Nullable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@AutoValue
@RegisterForReflection
@JsonPropertyOrder({"id", "title", "author", "content", "updateTime", "creationTime"})
public abstract class PostTO {

    @Nullable
    public abstract UUID getId();

    @NotNull
    @Size(min = 5, max = 256)
    public abstract String getTitle();

    @NotNull
    @Size(min = 5, max = 256)
    public abstract String getAuthor();

    @NotNull
    @Size(min = 5, max = 20000)
    public abstract String getContent();

    @Nullable
    public abstract List<CommentTO> getComments();

    @Null
    @Nullable

    public abstract ZonedDateTime getUpdateTime();

    @Null
    @Nullable
    public abstract ZonedDateTime getCreationTime();

    @JsonCreator
    public static PostTO create(@JsonProperty("id") final UUID id,
                                @JsonProperty("title") final String title,
                                @JsonProperty("author") final String author,
                                @JsonProperty("content") final String content,
                                @JsonProperty("comments") final List<CommentTO> comments,
                                @JsonProperty("updateTime") final ZonedDateTime updateTime,
                                @JsonProperty("creationTime") final ZonedDateTime creationTime) {
        return builder()
                .id(id)
                .title(title)
                .author(author)
                .content(content)
                .comments(comments)
                .updateTime(updateTime)
                .creationTime(creationTime)
                .build();

    }

    public static Builder builder() {
        return new AutoValue_PostTO.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder id(UUID newId);

        public abstract Builder title(String newTitle);

        public abstract Builder author(String newAuthor);

        public abstract Builder content(String newContent);

        public abstract Builder comments(@Valid List<CommentTO> comments);

        public abstract Builder updateTime(ZonedDateTime newUpdateTime);

        public abstract Builder creationTime(ZonedDateTime newCreationTime);

        public abstract PostTO build();
    }
}
