package ro.blogging.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.auto.value.AutoValue;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.reactivex.annotations.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@AutoValue
@RegisterForReflection
@JsonPropertyOrder({"page", "size", "sortProperties", "ascending"})
public abstract class PageRequestTO {

    @NotNull
    @Min(0)
    public abstract int getPage();

    @NotNull
    @Min(1)
    public abstract int getSize();

    @Size(min = 1)
    @Nullable
    public abstract Set<String> getSortProperties();

    @Nullable
    public abstract Boolean getAscending();

    @JsonCreator
    public static PageRequestTO create(@JsonProperty("page") final int page,
                                       @JsonProperty("size") final int size,
                                       @JsonProperty("sortProperties") final Set<String> sortProperties,
                                       @JsonProperty("ascending") final Boolean ascending) {
        return new AutoValue_PageRequestTO.Builder()
                .page(page)
                .size(size)
                .sortProperties(sortProperties)
                .ascending(ascending)
                .build();
    }

    public static PageRequestTO.Builder builder() {
        return new AutoValue_PageRequestTO.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {

        public abstract PageRequestTO.Builder page(final int size);

        public abstract PageRequestTO.Builder size(final int page);

        public abstract PageRequestTO.Builder sortProperties(final Set<String> sortProperties);

        public abstract PageRequestTO.Builder ascending(final Boolean isAscending);

        public abstract PageRequestTO build();
    }
}
