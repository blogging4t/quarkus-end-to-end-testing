package ro.blogging.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.auto.value.AutoValue;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.UUID;

@AutoValue
@RegisterForReflection
@JsonPropertyOrder({"id", "title", "author", "content", "updateTime", "creationTime"})
public abstract class CommentTO {

    @Nullable
    public abstract UUID getId();

    @NotNull
    @Size(min = 5, max = 256)
    public abstract String getTitle();

    @NotNull
    @Size(min = 5, max = 256)
    public abstract String getAuthor();

    @NotNull
    @Size(min = 5, max = 600)
    public abstract String getContent();

    @Nullable
    public abstract UUID getPostId();

    @Null
    @Nullable
    public abstract ZonedDateTime getUpdateTime();

    @Null
    @Nullable
    public abstract ZonedDateTime getCreationTime();

    @JsonCreator
    public static CommentTO create(@JsonProperty("id") final UUID id,
                                   @JsonProperty("title") final String title,
                                   @JsonProperty("author") final String author,
                                   @JsonProperty("content") final String content,
                                   @JsonProperty("postId") final UUID postId,
                                   @JsonProperty("updateTime") final ZonedDateTime updateTime,
                                   @JsonProperty("creationTime") final ZonedDateTime creationTime) {
        return builder()
                .id(id)
                .title(title)
                .author(author)
                .content(content)
                .postId(postId)
                .updateTime(updateTime)
                .creationTime(creationTime)
                .build();
    }


    public static Builder builder() {
        return new AutoValue_CommentTO.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder id(final UUID id);

        public abstract Builder title(final String title);

        public abstract Builder author(final String author);

        public abstract Builder content(final String content);

        public abstract Builder postId(final UUID postId);

        public abstract Builder updateTime(final ZonedDateTime newUpdateTime);

        public abstract Builder creationTime(final ZonedDateTime newCreationTime);

        public abstract CommentTO build();
    }
}
