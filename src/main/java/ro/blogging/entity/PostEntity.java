package ro.blogging.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(schema = "blog", name = "posts")
public class PostEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "author", nullable = false, updatable = false)
    private String author;

    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(mappedBy = "postEntity", fetch = FetchType.LAZY, cascade = {
            CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE
    }, orphanRemoval = true)
    private List<CommentEntity> comments;

    @Column(name = "update_time")
    @UpdateTimestamp
    private ZonedDateTime updateTime;

    @Column(name = "creation_time", nullable = false)
    @CreationTimestamp
    private ZonedDateTime creationTime;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(final ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(final ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public void setComments(final List<CommentEntity> commentEntities) {
        if (!CollectionUtils.isEmpty(commentEntities)) {
            commentEntities.forEach(commentEntity -> commentEntity.setPostEntity(this));
        }
        comments = commentEntities;
    }

    public List<CommentEntity> getComments() {
        return comments;
    }
}
