package ro.blogging.resource;

import ro.blogging.dto.CommentTO;
import ro.blogging.dto.GetCommentsRequestTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path(CommentResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface CommentResource {
    String PATH = "/comments";

    @POST
    Response save(@Valid @NotNull CommentTO comment);

    @POST
    @Path("/for-post")
    Response getForPost(@Valid @NotNull GetCommentsRequestTO request);

    @PUT
    Response update(@Valid @NotNull final CommentTO comment);

    @DELETE
    Response delete(@QueryParam("id") final UUID id);
}
