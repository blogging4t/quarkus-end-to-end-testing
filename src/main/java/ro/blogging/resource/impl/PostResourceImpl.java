package ro.blogging.resource.impl;

import ro.blogging.dto.PageRequestTO;
import ro.blogging.dto.PostTO;
import ro.blogging.resource.PostResource;
import ro.blogging.service.PostService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path(PostResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResourceImpl implements PostResource {
    private final PostService postService;

    @Inject
    public PostResourceImpl(final PostService postService) {
        this.postService = postService;
    }


    @Override
    public Response save(@Valid @NotNull final PostTO post) {
        return Response
                .status(Response.Status.CREATED)
                .entity(postService.save(post))
                .build();

    }

    @Override
    public Response getPosts(@Valid @NotNull final PageRequestTO pageRequest) {
        return Response
                .status(Response.Status.OK)
                .entity(postService.getPosts(pageRequest))
                .build();
    }


    @Override
    public Response update(@Valid @NotNull final PostTO post) {
        final int isUpdated = postService.update(post);
        final Response.Status status = isUpdated > 0 ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response
                .status(status)
                .build();
    }

    @Override
    public Response deletePost(final UUID id) {
        postService.delete(id);
        return Response
                .ok()
                .build();
    }
}
