package ro.blogging.resource.impl;

import ro.blogging.dto.CommentTO;
import ro.blogging.dto.GetCommentsRequestTO;
import ro.blogging.resource.CommentResource;
import ro.blogging.service.CommentService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path(CommentResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResourceImpl implements CommentResource {
    private final CommentService commentService;

    @Inject
    public CommentResourceImpl(final CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public Response save(@Valid @NotNull final CommentTO comment) {
        return Response
                .status(Response.Status.CREATED)
                .entity(commentService.save(comment))
                .build();
    }

    @Override
    public Response getForPost(final GetCommentsRequestTO request) {
        return Response
                .ok(commentService.getForPost(request))
                .build();
    }

    @Override
    public Response update(@Valid @NotNull final CommentTO comment) {
        final int updateStatus = commentService.update(comment);
        final Response.Status httpStatus = updateStatus > 0 ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response
                .status(httpStatus)
                .build();
    }

    @Override
    public Response delete(final UUID id) {
        commentService.deleteComment(id);
        return Response.
                ok()
                .build();
    }
}
