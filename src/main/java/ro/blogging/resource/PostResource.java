package ro.blogging.resource;

import ro.blogging.dto.PageRequestTO;
import ro.blogging.dto.PostTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path(PostResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface PostResource {
    String PATH = "/posts";

    @POST
    Response save(@Valid @NotNull PostTO post);

    @POST
    @Path("/get")
    Response getPosts(@Valid @NotNull PageRequestTO pageRequest);

    @PUT
    Response update(@Valid @NotNull PostTO post);

    @DELETE
    Response deletePost(@QueryParam("id") final UUID id);
}
