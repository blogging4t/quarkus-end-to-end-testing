package ro.blogging.service.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.blogging.dto.PostTO;
import ro.blogging.entity.PostEntity;

@Mapper(componentModel = "cdi")
public interface PostMapper {

    @Mapping(target = "comments", ignore = true)
    PostEntity toEntity(final PostTO post);

    @Mapping(target = "comments", ignore = true)
    PostTO toResource(final PostEntity entity);

    PostTO toResourceWithComments(final PostEntity entity);
}

