package ro.blogging.service.mappers;

import com.google.common.collect.Iterables;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import ro.blogging.dto.PageRequestTO;

import javax.enterprise.context.ApplicationScoped;

import static com.google.common.base.Preconditions.checkNotNull;

@ApplicationScoped
public class PageRequestMapper {

    public PageRequest toPageRequest(final PageRequestTO request) {
        checkNotNull(request);
        if (CollectionUtils.isEmpty(request.getSortProperties())) {
            return PageRequest.of(request.getPage(), request.getSize());
        }

        final Sort sort = Sort.by(Iterables.toArray(request.getSortProperties(), String.class));
        final boolean ascending = request.getAscending() == null ? false : request.getAscending();
        return PageRequest.of(request.getPage(), request.getSize(),
                ascending ? sort.ascending() : sort.descending()
        );
    }
}
