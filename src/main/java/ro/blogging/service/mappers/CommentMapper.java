package ro.blogging.service.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.blogging.dto.CommentTO;
import ro.blogging.entity.CommentEntity;

@Mapper(componentModel = "cdi")
public interface CommentMapper {
    CommentEntity toEntity(final CommentTO comment);

    @Mapping(target = "postId", source = "postEntity.id")
    CommentTO toResource(final CommentEntity entity);
}
