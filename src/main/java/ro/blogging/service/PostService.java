package ro.blogging.service;

import org.springframework.data.domain.Page;
import ro.blogging.dto.PageRequestTO;
import ro.blogging.dto.PostTO;

import java.util.UUID;

public interface PostService {
    PostTO save(final PostTO post);

    Page<PostTO> getPosts(PageRequestTO pageRequest);

    int update(final PostTO post);

    void delete(final UUID postId);
}
