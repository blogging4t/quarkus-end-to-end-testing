package ro.blogging.service.impl;

import com.google.common.collect.ImmutableList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import ro.blogging.dto.PageRequestTO;
import ro.blogging.dto.PostTO;
import ro.blogging.entity.PostEntity;
import ro.blogging.repository.PostRepository;
import ro.blogging.service.PostService;
import ro.blogging.service.mappers.PageRequestMapper;
import ro.blogging.service.mappers.PostMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final PageRequestMapper pageRequestMapper;

    @Inject
    public PostServiceImpl(final PostRepository postRepository, final PostMapper postMapper, final PageRequestMapper pageRequestMapper) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.pageRequestMapper = pageRequestMapper;
    }

    @Override
    public PostTO save(final PostTO post) {
        final PostEntity savedEntity = postRepository.save(postMapper.toEntity(post));
        return postMapper.toResource(savedEntity);
    }

    @Override
    public Page<PostTO> getPosts(final PageRequestTO pageRequest) {
        final Page<PostEntity> all = postRepository.findAll(pageRequestMapper.toPageRequest(pageRequest));
        final List<PostTO> posts = all.stream().parallel()
                .map(postMapper::toResource)
                .collect(ImmutableList.toImmutableList());
        return new PageImpl<>(posts, all.getPageable(), all.getTotalElements());
    }

    @Override
    public int update(final PostTO post) {
        return postRepository.updatePost(post.getTitle(), post.getContent(), post.getAuthor(), ZonedDateTime.now(), post.getId());
    }

    @Override
    public void delete(final UUID postId) {
        postRepository.deleteById(postId);
    }
}
