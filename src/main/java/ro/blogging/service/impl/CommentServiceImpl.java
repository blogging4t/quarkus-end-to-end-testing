package ro.blogging.service.impl;

import com.google.common.collect.ImmutableList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import ro.blogging.dto.CommentTO;
import ro.blogging.dto.GetCommentsRequestTO;
import ro.blogging.entity.CommentEntity;
import ro.blogging.entity.PostEntity;
import ro.blogging.repository.CommentRepository;
import ro.blogging.repository.PostRepository;
import ro.blogging.service.CommentService;
import ro.blogging.service.mappers.CommentMapper;
import ro.blogging.service.mappers.PageRequestMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

@ApplicationScoped
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final PostRepository postRepository;
    private final PageRequestMapper pageRequestMapper;

    @Inject
    public CommentServiceImpl(final CommentRepository commentRepository,
                              final CommentMapper commentMapper,
                              final PostRepository postRepository,
                              final PageRequestMapper pageRequestMapper) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.postRepository = postRepository;
        this.pageRequestMapper = pageRequestMapper;
    }

    @Override
    public CommentTO save(final CommentTO comment) {
        final PostEntity postEntity = postRepository.findById(checkNotNull(comment.getPostId()))
                .orElseThrow(IllegalArgumentException::new);
        final CommentEntity entity = commentMapper.toEntity(comment);
        entity.setPostEntity(postEntity);
        return commentMapper.toResource(commentRepository.save(entity));
    }

    @Override
    public Page<CommentTO> getForPost(final GetCommentsRequestTO request) {
        final PostEntity postEntity = postRepository.findById(request.getPostId()).orElseThrow(IllegalArgumentException::new);
        final Page<CommentEntity> commentEntities = commentRepository.getAllByPostEntity(
                postEntity,
                pageRequestMapper.toPageRequest(request.getPageRequest())
        );
        final List<CommentTO> comments = commentEntities.stream()
                .parallel()
                .map(commentMapper::toResource)
                .collect(ImmutableList.toImmutableList());
        return new PageImpl<>(comments, commentEntities.getPageable(), commentEntities.getTotalElements());
    }

    @Override
    public int update(final CommentTO comment) {

        return commentRepository.updateComment(comment.getTitle(),
                comment.getContent(),
                comment.getAuthor(),
                ZonedDateTime.now(),
                comment.getId());
    }

    @Override
    public void deleteComment(final UUID commentId) {
        commentRepository.deleteById(commentId);
    }
}
