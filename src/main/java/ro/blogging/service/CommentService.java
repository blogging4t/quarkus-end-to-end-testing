package ro.blogging.service;

import org.springframework.data.domain.Page;
import ro.blogging.dto.CommentTO;
import ro.blogging.dto.GetCommentsRequestTO;

import java.util.UUID;

public interface CommentService {
    CommentTO save(final CommentTO comment);

    Page getForPost(final GetCommentsRequestTO request);

    int update(final CommentTO comment);

    void deleteComment(final UUID commentId);
}
