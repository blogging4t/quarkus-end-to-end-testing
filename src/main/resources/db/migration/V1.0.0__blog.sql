create schema if not exists blog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table if not exists blog.posts
(
    id            uuid primary key default uuid_generate_v4(),
    title         varchar(255) not null,
    author        varchar(255) not null,
    content       text         not null,
    creation_time timestamp    not null,
    update_time   timestamp
);

create table if not exists blog.comments
(
    id            uuid primary key default uuid_generate_v4(),
    title         varchar(255) not null,
    content       text         not null,
    author        varchar(255) not null,
    creation_time timestamp    not null,
    update_time   timestamp,
    post_id       uuid         not null references blog.posts ON DELETE CASCADE not null
);
