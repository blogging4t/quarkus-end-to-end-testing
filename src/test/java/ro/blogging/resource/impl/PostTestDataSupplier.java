package ro.blogging.resource.impl;

import ro.blogging.dto.PostTO;

public interface PostTestDataSupplier {

    PostTO VALID_POST = PostTO.builder()
            .title("Test Post title")
            .author("Test author")
            .content("Test Content")
            .build();


    PostTO POST_WITH_TOO_LESS_CHARACTERS_IN_TITLE = PostTO.builder()
            .title("Tes")
            .author("Test author")
            .content("Test Content")
            .build();

    PostTO POST_WITH_TOO_LESS_CHARACTERS_IN_AUTHOR = PostTO.builder()
            .title("Test Post title")
            .author("Tes")
            .content("Test Content")
            .build();

    PostTO POST_WITH_TOO_LESS_CHARACTERS_IN_CONTENT = PostTO.builder()
            .title("Test Post title")
            .author("Test author")
            .content("Tes")
            .build();
}
