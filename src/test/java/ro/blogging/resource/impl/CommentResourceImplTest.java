package ro.blogging.resource.impl;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.Header;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import ro.blogging.dto.CommentTO;
import ro.blogging.dto.GetCommentsRequestTO;
import ro.blogging.dto.PageRequestTO;
import ro.blogging.entity.CommentEntity;
import ro.blogging.repository.CommentRepository;
import ro.blogging.repository.PostRepository;
import ro.blogging.resource.CommentResource;
import ro.blogging.service.CommentService;
import ro.blogging.service.PostService;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class CommentResourceImplTest {
    private static final String CONTENT_TYPE = "Content-Type";
    private static final Header HEADER = new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON);

    @Inject
    CommentService commentService;

    @Inject
    CommentRepository commentRepository;

    @Inject
    PostService postService;

    @Inject
    PostRepository postRepository;


    @AfterEach
    public void tearDown() {
        commentRepository.deleteAll();
        postRepository.deleteAll();
    }

    @Test
    public void succeedOnSavingComment() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = createCommentForPost(postId);

        final CommentTO comment = given()
                .body(validComment)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH)
                .then()
                .statusCode(201)
                .extract()
                .as(CommentTO.class);

        assertThat(comment).isNotNull();
        assertThat(comment.getId()).isNotNull();
        assertThat(comment.getAuthor()).isEqualTo(comment.getAuthor());
        assertThat(comment.getTitle()).isEqualTo(comment.getTitle());
        assertThat(comment.getContent()).isEqualTo(comment.getContent());
        assertThat(comment.getPostId()).isEqualTo(postId);
        assertThat(comment.getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
        assertThat(comment.getUpdateTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
    }

    @Test
    public void failOnSavingWithCustomId() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final UUID customCommentId = UUID.randomUUID();
        final CommentTO validComment = CommentTO.builder()
                .id(customCommentId)
                .title("Test comment title")
                .author("Test Comment author")
                .content("Test Content")
                .postId(postId)
                .build();

        final CommentTO comment = given()
                .body(validComment)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH)
                .then()
                .statusCode(201)
                .extract()
                .as(CommentTO.class);

        assertThat(comment).isNotNull();
        assertThat(comment.getId()).isNotEqualTo(customCommentId);
    }

    @Test
    public void failOnSavingCommentWithTooLessCharactersInTitle() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = CommentTO.builder()
                .title("Tes")
                .author("Test Comment author")
                .content("Test Comment Content")
                .postId(postId)
                .build();

        given()
                .body(validComment)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void failOnSavingCommentWithTooLessCharactersInAuthor() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = CommentTO.builder()
                .title("Test Comment title")
                .author("Tes")
                .content("Test Comment Content")
                .postId(postId)
                .build();

        given()
                .body(validComment)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void failOnSavingCommentWithTooLessCharactersInContent() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = CommentTO.builder()
                .title("Test Comment title")
                .author("Test Comment author")
                .content("Tes")
                .postId(postId)
                .build();

        given()
                .body(validComment)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH)
                .then()
                .statusCode(400);
    }


    @Test
    public void succeedOnGettingCommentsForPost() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = createCommentForPost(postId);
        commentService.save(validComment);
        commentService.save(validComment);
        final GetCommentsRequestTO request = createGetRequestForPostId(postId);

        final List<CommentTO> comments = given()
                .body(request)
                .header(HEADER)
                .when()
                .post(CommentResource.PATH + "/for-post")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList("content", CommentTO.class);

        assertThat(comments).isNotNull();
        assertThat(comments.size()).isEqualTo(2);
        assertThat(comments.get(0).getId()).isNotNull();
        assertThat(comments.get(0).getTitle()).isEqualTo(validComment.getTitle());
        assertThat(comments.get(0).getAuthor()).isEqualTo(validComment.getAuthor());
        assertThat(comments.get(0).getContent()).isEqualTo(validComment.getContent());
        assertThat(comments.get(0).getPostId()).isEqualTo(postId);
        assertThat(comments.get(0).getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
        assertThat(comments.get(0).getUpdateTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
    }


    @Test
    public void succeedOnUpdatingComment() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = createCommentForPost(postId);
        final UUID commentId = commentService.save(validComment).getId();

        final CommentTO commentToUpdate = CommentTO.builder()
                .id(commentId)
                .title(validComment.getTitle())
                .author(validComment.getAuthor())
                .content(validComment.getContent())
                .build();

        given()
                .body(commentToUpdate)
                .header(HEADER)
                .when()
                .put(CommentResource.PATH)
                .then()
                .statusCode(200);
        final List<CommentTO> comments = commentService.getForPost(createGetRequestForPostId(postId)).getContent();

        assertThat(comments).isNotNull();
        assertThat(comments.size()).isEqualTo(1);
        assertThat(comments.get(0).getId()).isEqualTo(commentId);
        assertThat(comments.get(0).getTitle()).isEqualTo(commentToUpdate.getTitle());
        assertThat(comments.get(0).getAuthor()).isEqualTo(commentToUpdate.getAuthor());
        assertThat(comments.get(0).getContent()).isEqualTo(commentToUpdate.getContent());
        assertThat(comments.get(0).getPostId()).isEqualTo(postId);
        assertThat(comments.get(0).getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
        assertThat(comments.get(0).getUpdateTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
    }

    @Test
    public void succeedOnDelete() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();
        final CommentTO validComment = createCommentForPost(postId);
        final UUID commentId = commentService.save(validComment).getId();

        given()
                .param("id", commentId)
                .header(HEADER)
                .when()
                .delete(CommentResource.PATH)
                .then()
                .statusCode(200);
        final Iterable<CommentEntity> all = commentRepository.findAll();

        assertThat(all).isEmpty();
    }


    private CommentTO createCommentForPost(final UUID postId) {
        return CommentTO.builder()
                .title("Test comment title")
                .author("Test Comment author")
                .content("Test Content")
                .postId(postId)
                .build();
    }

    private GetCommentsRequestTO createGetRequestForPostId(final UUID postId) {
        return GetCommentsRequestTO.builder()
                .postId(postId)
                .pageRequest(PageRequestTO.builder()
                        .page(0)
                        .size(2)
                        .build())
                .build();
    }
}