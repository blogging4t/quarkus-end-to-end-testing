package ro.blogging.resource.impl;

import com.google.common.collect.ImmutableSet;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.Header;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import ro.blogging.dto.PageRequestTO;
import ro.blogging.dto.PostTO;
import ro.blogging.repository.PostRepository;
import ro.blogging.resource.PostResource;
import ro.blogging.service.PostService;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class PostResourceImplTest {
    private static final String CONTENT_TYPE = "Content-Type";
    private static final Header HEADER = new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON);

    @Inject
    PostRepository postRepository;
    @Inject
    PostService postService;

    @AfterEach
    public void tearDown() {
        postRepository.deleteAll();
    }

    @Test
    public void succeedOnSavingPost() {
        final PostTO savedPost = given()
                .body(PostTestDataSupplier.VALID_POST)
                .header(HEADER)
                .when()
                .post(PostResource.PATH)
                .then()
                .statusCode(201)
                .extract()
                .as(PostTO.class);

        assertThat(savedPost).isNotNull();
        assertThat(savedPost.getId()).isNotNull();
        assertThat(savedPost.getAuthor()).isEqualTo(PostTestDataSupplier.VALID_POST.getAuthor());
        assertThat(savedPost.getTitle()).isEqualTo(PostTestDataSupplier.VALID_POST.getTitle());
        assertThat(savedPost.getContent()).isEqualTo(PostTestDataSupplier.VALID_POST.getContent());
        assertThat(savedPost.getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
        assertThat(savedPost.getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
    }

    @Test
    public void failOnCreatingPostWithCustomId() {
        final UUID customId = UUID.randomUUID();
        final PostTO savedPost = given()
                .body(PostTO.builder()
                        .id(customId)
                        .title("Test Post title")
                        .author("Test author")
                        .content("Test Content")
                        .build())
                .header(HEADER)
                .when()
                .post(PostResource.PATH)
                .then()
                .statusCode(201)
                .extract()
                .as(PostTO.class);

        assertThat(savedPost).isNotNull();
        assertThat(savedPost.getId()).isNotEqualTo(customId);
    }


    @Test
    public void failOnSavingPostWithTooLessCharactersInTitle() {
        given()
                .body(PostTestDataSupplier.POST_WITH_TOO_LESS_CHARACTERS_IN_TITLE)
                .header(HEADER)
                .when()
                .post(PostResource.PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void failOnSavingPostWithTooLessCharactersInAuthor() {
        given()
                .body(PostTestDataSupplier.POST_WITH_TOO_LESS_CHARACTERS_IN_AUTHOR)
                .header(HEADER)
                .when()
                .post(PostResource.PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void failOnSavingPostWithTooLessCharactersInContent() {
        given()
                .body(PostTestDataSupplier.POST_WITH_TOO_LESS_CHARACTERS_IN_CONTENT)
                .header(HEADER)
                .when()
                .post(PostResource.PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void succeedOnGettingPosts() {
        postService.save(PostTestDataSupplier.VALID_POST);
        postService.save(PostTestDataSupplier.VALID_POST);

        final PageRequestTO pageRequest = PageRequestTO.builder()
                .page(0)
                .size(2)
                .sortProperties(ImmutableSet.of("updateTime"))
                .build();

        final List<PostTO> posts = given()
                .body(pageRequest)
                .header(HEADER)
                .when()
                .post(PostResource.PATH + "/get")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList("content", PostTO.class);

        assertThat(posts).isNotNull();
        assertThat(posts.size()).isEqualTo(2);
        assertThat(posts.get(0).getTitle()).isEqualTo(PostTestDataSupplier.VALID_POST.getTitle());
        assertThat(posts.get(0).getAuthor()).isEqualTo(PostTestDataSupplier.VALID_POST.getAuthor());
        assertThat(posts.get(0).getContent()).isEqualTo(PostTestDataSupplier.VALID_POST.getContent());
        assertThat(posts.get(0).getCreationTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
        assertThat(posts.get(0).getUpdateTime()).isBeforeOrEqualTo(ZonedDateTime.now(Clock.systemUTC()).plusMinutes(1));
    }

    @Test
    public void failOnGettingPostsForNegativePage() {
        final PageRequestTO pageRequest = PageRequestTO.builder()
                .page(-1)
                .size(2)
                .sortProperties(ImmutableSet.of("updateTime"))
                .build();

        given()
                .body(pageRequest)
                .header(HEADER)
                .when()
                .post(PostResource.PATH + "/get")
                .then()
                .statusCode(400);
    }

    @Test
    public void failOnGettingPostsForNegativeSize() {
        final PageRequestTO pageRequest = PageRequestTO.builder()
                .page(0)
                .size(-1)
                .sortProperties(ImmutableSet.of("updateTime"))
                .build();

        given()
                .body(pageRequest)
                .header(HEADER)
                .when()
                .post(PostResource.PATH + "/get")
                .then()
                .statusCode(400);
    }

    @Test
    public void succeedOnUpdatingPost() {
        final PostTO savedPost = postService.save(PostTestDataSupplier.VALID_POST);

        final PostTO updatedPost = PostTO.builder()
                .id(savedPost.getId())
                .title("Test updated title")
                .author("Test updated author")
                .content("Test updated content")
                .build();

        given()
                .body(updatedPost)
                .header(HEADER)
                .when()
                .put(PostResource.PATH)
                .then()
                .statusCode(200);

        final List<PostTO> posts = postService.getPosts(PageRequestTO.builder().page(0).size(1).build()).getContent();

        assertThat(posts).isNotNull();
        assertThat(posts.size()).isEqualTo(1);
        assertThat(posts.get(0).getTitle()).isEqualTo(updatedPost.getTitle());
        assertThat(posts.get(0).getAuthor()).isEqualTo(updatedPost.getAuthor());
        assertThat(posts.get(0).getContent()).isEqualTo(updatedPost.getContent());
        assertThat(posts.get(0).getCreationTime()).isEqualTo(savedPost.getCreationTime());
        assertThat(posts.get(0).getUpdateTime()).isBefore(ZonedDateTime.now());
    }

    @Test
    public void succeedOnDeletingPost() {
        final UUID postId = postService.save(PostTestDataSupplier.VALID_POST).getId();

        given()
                .param("id", postId)
                .header(HEADER)
                .when()
                .delete(PostResource.PATH)
                .then()
                .statusCode(200);

        final List<PostTO> posts = postService.getPosts(PageRequestTO.builder()
                .page(0)
                .size(1)
                .build()
        ).getContent();

        assertThat(posts).isNotNull();
        assertThat(posts).isEmpty();
    }
}